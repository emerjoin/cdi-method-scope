# CDI Method Scope
Well, this is a CDI extension that allows you to have beans that live only within the execution of a method.

## Maven coordinates
Have a look at the pom.xml snippet below:
```xml

<dependencies>
    <dependency>
        <groupId>org.emerjoin</groupId>
        <artifactId>cdi-method-scope</artifactId>
        <version>0.1.0</version>
    </dependency>
</dependencies>

<repositories>
    <repository>
         <id>bintray-emerjoin-maven</id>
         <name>bintray</name>
         <url>https://dl.bintray.com/emerjoin/maven</url>
    </repository>
</repositories>


```

## User Manual
There are three things you need to learn:
* How to activate the API
* How to create method scoped beans
* How to enable the method context
* How to isolate a method call

### Activating the API
Just declare the interceptor class in your __beans.xml__:
```java
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://xmlns.jcp.org/xml/ns/javaee"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/beans_2_0.xsd" bean-discovery-mode="all" version="2.0">
    <interceptors>
        <class>org.emerjoin.cdi.methodcontext.Interceptor</class>
    </interceptors>
</beans>
```

### Creating Method Scoped beans
You just need to annotate the bean class or producer method with __@MethodScoped__.


#### Class
```java
@MethodScoped
public class SomeObject {
    
    //properties and methods   

}
```

#### Producer
```java
public class ProducerBean {
    
    //properties and methods  
 
    @Produces @MethodScoped
    public SomeObject produceBean(){
        //TODO: Create the object
    }

}
```

### Enabling the method context
The method context can be enabled either by annotating the class that contains
the methods to be enabled or by annotating the methods directly with __@MethodScopeEnabled__. <br>

The scope is only activated when an enabled method is invoked.

#### All methods of bean
Just annotate the class and all it's methods will be enabled:
```java
@MethodScopeEnabled
@ApplicationScoped
public class SomeBean {
    
    @Inject
    private SomeObject someObject;

    public void method1(){
        //TODO: do something
    }

    public void method2(){
        //TODO: do something
    }

}
```

#### Specific methods of a bean
Just annotate the methods you want to enable:
```java
@ApplicationScoped
public class SomeBean {
    
    @Inject
    private SomeObject someObject;
    
    @MethodScopeEnabled
    public void method1(){
        //TODO: do something
    }

    public void method2(){
        //TODO: do something
    }

}
```

### Isolation
By default, When a method annotated with __@MethodScopeEnabled__ is invoked, a context is 
created and associated with that method call, unless there is already a context associated 
to the method call. This makes it possible to have a call chain in which __@MethodScoped__ beans are
being shared. <br>

But sometimes you want a method to be fully isolated, not sharing it's __@MethodScoped__ beans, nor reusing 
beans from parent method contexts.

How do you achieve that? It is as simple as follows:
```java
@MethodScopeEnabled(isolate=true);
```

