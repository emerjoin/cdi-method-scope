package org.emerjoin.cdi.methodcontext;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Optional;

public class ThreadState {

    private static final ThreadLocal<ThreadState> STATE_THREAD_LOCAL = new ThreadLocal<>();

    private Deque<ContextInstance> contextInstances = new ArrayDeque<>();

    private ThreadState(){

    }

    public boolean isContextActive(){

        return !contextInstances.isEmpty();

    }

    Optional<ContextInstance> currentContext(){
        if(contextInstances.isEmpty())
            return Optional.empty();
        return Optional.of(contextInstances
                .peek());
    }

    int countContexts(){
        return contextInstances.size();
    }

    ContextInstance startNewContext(){
        ContextInstance contextInstance = new ContextInstance();
        contextInstances.add(contextInstance);
        return contextInstance;
    }

    void endCurrentContext(){
        if(contextInstances.isEmpty())
            return;
        ContextInstance instance = contextInstances.pop();
        instance.destroy();
    }

    static ThreadState current(){
        ThreadState state = STATE_THREAD_LOCAL.get();
        if(state==null){
            state = new ThreadState();
            STATE_THREAD_LOCAL.set(state);
        }
        return state;
    }

}
