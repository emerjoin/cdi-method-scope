package org.emerjoin.cdi.methodcontext;

import java.util.Optional;
import java.util.UUID;

public class ContextInstance {

    private String id;
    private ContextObjectsHolder holder = new ContextObjectsHolder();

    ContextInstance(){
        id = UUID.randomUUID().toString();
    }

    String getId() {
        return id;
    }

    void destroy(){
        this.holder.destroyAll();
    }


    Optional<ScopedInstance> getBeanInstance(Class beanClazz){
        return this.holder.get(beanClazz);
    }

    void putBeanInstance(ScopedInstance scopedInstance){

        this.holder.put(scopedInstance);

    }


}
