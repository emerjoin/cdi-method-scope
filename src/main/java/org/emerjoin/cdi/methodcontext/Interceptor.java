package org.emerjoin.cdi.methodcontext;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.enterprise.context.Dependent;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import java.util.Optional;

@MethodScopeEnabled
@javax.interceptor.Interceptor
@Dependent
public class Interceptor {

    private static final Logger LOGGER = LogManager.getLogger(Interceptor.class);

    @AroundInvoke
    public Object intercept(InvocationContext ctx) throws Exception {
        MethodScopeEnabled boundary = ctx.getMethod().getAnnotation(MethodScopeEnabled.class);
        ThreadState threadState = ThreadState.current();
        boolean started = false;
        try {
            if (!threadState.isContextActive()) {
                LOGGER.info("Starting a new Method Context");
                ContextInstance contextInstance = threadState.startNewContext();
                LOGGER.info("Context started successfully [" + contextInstance.getId() + "]");
                started = true;
            } else {
                if (boundary.isolate()) {
                    LOGGER.info("Starting a new isolated Method Context");
                    ContextInstance contextInstance = threadState.startNewContext();
                    LOGGER.info("Isolated Context started successfully [" + contextInstance.getId() + "]");
                    started = true;
                }
            }
            return ctx.proceed();
        }finally {
            if(started) {
                Optional<ContextInstance> optionalContextInstance = threadState.currentContext();
                ContextInstance contextInstance = optionalContextInstance.get();
                LOGGER.info("Ending current Method Context ["+contextInstance.getId()+"]");
                threadState.endCurrentContext();
            }
        }
    }

}
